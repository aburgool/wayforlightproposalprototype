/**
 * Copyright 2011-2015 CELLS
 *
 * This file is part of CELLS UserOffice.
 *
 * CELLS UserOffice is free software: you can redistribute it and or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CELLS UserOffice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CELLS UserOffice.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.cells.utils

String wayForLightTestProposalFile = 'src/main/resources/WayForLightTestProposal.xml'
def wayForLightTestProposalParser = new XmlParser().parse(wayForLightTestProposalFile)
def wayForLightTestProposalSlurper = new XmlSlurper().parse(wayForLightTestProposalFile)

wayForLightTestProposalParser.each{ proposal ->
  println proposal.General.Title.text()
}

wayForLightTestProposalSlurper.each{ proposal ->
  println proposal.General[0].Title.text()
}

wayForLightTestProposalParser.children().each{ 
  println it.name() 
}


